import 'package:flutter/material.dart';

class OpacityAnimation extends StatelessWidget {
  const OpacityAnimation(
      {super.key, required this.child, required this.animation});

  final Widget child;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) => Center(
        child: AnimatedBuilder(
          animation: animation,
          builder: (context, child) => Container(
            padding: const EdgeInsets.all(10),
            child: Opacity(
              opacity: animation.value,
              child: child,
            ),
          ),
          child: child,
        ),
      );
}
